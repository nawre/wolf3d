#include "wolf3d.h"

int	printkey(int key, void *rien)
{
	printf("%d \n", key);
	return (0);
}

int	main()
{
	int	x;
	int	y;
	int	col;
	x = 0;
	y = 0;
	t_env	*env;
	env = malloc(sizeof(t_env));
	env->mlx = mlx_init();
	env->win = mlx_new_window(env->mlx, 1000, 600, "TEST");
	mlx_hook(env->win, 2, 1L << 0, &printkey, NULL);
	mlx_loop(env->mlx);
}


/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keybinding.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/21 17:25:22 by erodrigu          #+#    #+#             */
/*   Updated: 2016/09/30 01:35:03 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		keybinding(int key, t_env *env)
{
	if (key == 0)
	{
		env->pl->x -= sin(env->pl->angle) * 10;
		env->pl->y += cos(env->pl->angle) * 10;
	}
	if (key == 1)
	{
		env->pl->x += cos(env->pl->angle) * 10;
		env->pl->y += sin(env->pl->angle) * 10;
	}
	if (key == 2)
	{
		env->pl->x += sin(env->pl->angle) * 10;
		env->pl->y -= cos(env->pl->angle) * 10;
	}
	if (key == 13)
	{
		env->pl->x -= cos(env->pl->angle) * 10;
		env->pl->y -= sin(env->pl->angle) * 10;
	}
	threading(env);
	return (0);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/12 13:27:10 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/22 14:32:20 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

int		put_in_line(char **line, char *buffer, char **save, int test)
{
	char	*temp;

	if (test == 42)
	{
		if (*save != NULL)
		{
			temp = ft_strsub(buffer, 0, ft_strchri(buffer, '\n'));
			*line = ft_strjoin(*save, temp);
			if (temp != NULL)
				free(temp);
			free(*save);
		}
		else
			*line = ft_strsub(buffer, 0, ft_strchri(buffer, '\n'));
		*save = ft_strdup(buffer + ft_strchri(buffer, '\n') + 1);
		return (1);
	}
	else if (*save != NULL && *save[0] != 0)
	{
		*line = *save;
		*save = NULL;
		return (1);
	}
	return (0);
}

int		put_in_perm(char **save, int fd, char **line)
{
	char	buf[BUFF_SIZE + 1];
	char	*temp;
	int		ret;

	ft_bzero(buf, BUFF_SIZE + 1);
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0 && ft_strchri(buf, '\n') < 0)
	{
		temp = (*save == NULL) ? ft_strdup(buf) : ft_strjoin(*save, buf);
		if (*save != NULL)
			free(*save);
		*save = temp;
		ft_bzero(buf, BUFF_SIZE + 1);
	}
	if (ret == -1)
		return (-1);
	if (ret == 0 && save != NULL)
		return (put_in_line(line, buf, save, 0));
	else
		return (put_in_line(line, buf, save, 42));
}

char	**find_fd(t_save *l_save, int const fd)
{
	if (l_save == NULL)
		return (NULL);
	while (l_save->next != NULL)
	{
		if (l_save->fd == fd)
			return (&l_save->save);
		l_save = l_save->next;
	}
	if (l_save->fd == fd)
		return (&l_save->save);
	return (NULL);
}

void	new_fd(t_save **l_save, int fd)
{
	t_save	*current;
	t_save	*new;

	current = *l_save;
	if (current == NULL)
	{
		current = (t_save*)malloc(sizeof(t_save));
		current->next = NULL;
		current->save = NULL;
		current->fd = fd;
		*l_save = current;
	}
	else
	{
		while (current->next != NULL)
			current = current->next;
		new = (t_save*)malloc(sizeof(t_save));
		new->next = NULL;
		new->save = NULL;
		new->fd = fd;
		current->next = new;
	}
}

int		get_next_line(int const fd, char **line)
{
	static t_save	*l_save = NULL;
	char			*tmp;
	char			**save;

	if (fd < 0 || !line || BUFF_SIZE < 0)
		return (-1);
	if (find_fd(l_save, fd) == NULL)
		new_fd(&l_save, fd);
	save = find_fd(l_save, fd);
	if (save != NULL && ft_strchri(*save, '\n') >= 0)
	{
		*line = ft_strsub(*save, 0, ft_strchri(*save, '\n'));
		tmp = ft_strdup(*save + ft_strchri(*save, '\n') + 1);
		if (*save != NULL)
			free(*save);
		*save = tmp;
	}
	else
		return (put_in_perm(save, fd, line));
	return (1);
}

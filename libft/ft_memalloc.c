/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/27 09:55:34 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/31 11:34:28 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memalloc(size_t size)
{
	char	*str;
	size_t	i;

	str = NULL;
	if (size == 0)
		return (str);
	str = (char *)malloc(sizeof(char) * size);
	i = 0;
	while (str && str[i])
	{
		str[i] = 0;
		i++;
	}
	return (str);
}

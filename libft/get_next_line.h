/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/05 03:10:55 by erodrigu          #+#    #+#             */
/*   Updated: 2016/02/18 14:00:17 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H
# define BUFF_SIZE 8

# include "libft.h"
# include <fcntl.h>

typedef struct		s_save
{
	char			*save;
	int				fd;
	struct s_save	*next;
}					t_save;

int					get_next_line(int const fd, char **line);
int					put_in_char(char **buff_perm, int fd);
int					perm_in_line(char **buff_perm, char **line);
void				new_fd(t_save **l_save, int fd);
char				**find_fd(t_save *l_save, int const fd);

#endif

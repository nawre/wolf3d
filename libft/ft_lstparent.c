/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstparent.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/20 07:37:38 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/20 19:38:00 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstparent(t_list *alst, t_list *now)
{
	t_list	*cursor;

	cursor = alst;
	while (cursor->next != now)
		cursor = cursor->next;
	return (cursor);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 00:07:02 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/11 06:18:52 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		i;
	int		deb;
	int		size;
	char	*str;

	i = 0;
	if (s == NULL)
		return (NULL);
	while (s[i] == ' ' || s[i] == '\n' || s[i] == '\t')
		i++;
	deb = i;
	i = ft_strlen(s);
	while ((s[i - 1] == ' ' || s[i - 1] == '\n' || s[i - 1] == '\t') && i > deb)
		i--;
	size = i - deb;
	str = ft_strnew(size);
	if (str == NULL)
		return (NULL);
	str = ft_strncpy(str, &s[deb], size);
	str[size] = 0;
	return (str);
}

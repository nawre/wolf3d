/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_trinum.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/08 09:31:53 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/14 23:22:11 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_trinum(int *tab, int size)
{
	int		temp;
	int		i;
	int		istri;

	istri = 0;
	while (istri == 0)
	{
		istri = 1;
		i = 0;
		while (i < size)
		{
			if (tab[i] > tab[i + 1])
			{
				temp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = temp;
				istri = 0;
			}
			i++;
		}
	}
}

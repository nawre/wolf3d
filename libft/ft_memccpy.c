/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 07:42:02 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/15 09:26:56 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*srci;
	unsigned char	*dsti;
	unsigned char	ci;

	ci = (unsigned char)c;
	dsti = (unsigned char*)dst;
	srci = (unsigned char*)src;
	i = 0;
	while (i < n)
	{
		dsti[i] = srci[i];
		if (srci[i] == ci)
			return (&(dst[i + 1]));
		i++;
	}
	return (NULL);
}

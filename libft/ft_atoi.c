/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 20:37:23 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/15 09:27:13 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static	int	ft_atooi(const char *str)
{
	int	i;
	int	res;

	res = 0;
	i = 0;
	while (str[i])
	{
		if (str[i] > 47 && str[i] < 58)
			res = (res * 10) + (str[i] - 48);
		else
			return (res);
		i++;
	}
	return (res);
}

int			ft_atoi(const char *str)
{
	int	i;
	int sign;
	int	res;

	i = 0;
	res = 0;
	sign = 0;
	while ((str[i] == ' ' || str[i] == '\n' || str[i] == '\v' || str[i] == '\t'
				|| str[i] == '\r' || str[i] == '\f') && str[i])
		i++;
	if (str[i] == '-' && res == 0 && sign == 0)
	{
		sign = -1;
		i++;
	}
	if (str[i] == '+' && res == 0 && sign == 0)
	{
		i++;
		sign = 1;
	}
	res = ft_atooi(&str[i]);
	if (sign < 0)
		return (-res);
	return (res);
}

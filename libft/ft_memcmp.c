/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 02:15:09 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/15 01:21:08 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t		i;
	char		*str1;
	char		*str2;
	int			res;

	str1 = (char*)s1;
	str2 = (char*)s2;
	i = 0;
	if (n == 0)
		return (0);
	while (n > 0 && str1 && str2)
	{
		res = (unsigned char)str1[i] - (unsigned char)str2[i];
		if (res != 0)
			return (res);
		i++;
		n--;
	}
	if (n != 0)
		res = (unsigned char)str1[i] - (unsigned char)str2[i];
	return (res);
}

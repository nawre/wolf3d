/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/10 21:11:13 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/11 09:13:30 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*newelem;

	newelem = malloc(sizeof(t_list));
	if (newelem == NULL)
		return (NULL);
	if (content == NULL)
	{
		newelem->content = NULL;
		newelem->content_size = 0;
		newelem->next = NULL;
		return (newelem);
	}
	newelem->content = ft_memalloc(content_size);
	newelem->content = ft_memcpy(newelem->content, content, content_size);
	newelem->content_size = content_size;
	newelem->next = NULL;
	return (newelem);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/09 12:36:32 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/09 15:36:39 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del) (void *, size_t))
{
	t_list	*lstct;
	t_list	*lstdel;

	lstct = *alst;
	while (lstct->next != NULL)
	{
		lstdel = lstct;
		lstct = lstct->next;
		del(lstdel->content, lstdel->content_size);
		free(lstdel);
	}
	del(lstct->content, lstct->content_size);
	free(lstct);
	*alst = NULL;
}

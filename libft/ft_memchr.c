/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 09:08:47 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/08 16:03:11 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	char	car;
	size_t	i;
	char	*str;

	str = (char*)s;
	i = 0;
	car = c;
	while (i < n)
	{
		if (str[i] == car)
			return (&str[i]);
		i++;
	}
	return (NULL);
}

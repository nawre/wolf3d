/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 16:41:09 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/15 09:46:25 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_intochar(int n)
{
	int c;

	c = 0;
	if (n <= 0)
		c++;
	while (n != 0)
	{
		n = n / 10;
		c++;
	}
	return (c);
}

char		*ft_itoa(int n)
{
	int		c;
	char	*str;

	c = ft_intochar(n);
	str = ft_strnew(c);
	if (n == -2147483648 && str != NULL)
		return (ft_strcpy(str, "-2147483648"));
	if (n == 0 && str != NULL)
	{
		str[0] = '0';
		return (str);
	}
	if (n < 0)
		n = -n;
	while (n > 0 && str != NULL)
	{
		str[c - 1] = (n % 10) + 48;
		n = n / 10;
		c--;
	}
	if (c == 1 && str != NULL)
		str[c - 1] = '-';
	return (str);
}

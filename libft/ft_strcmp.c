/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 15:45:59 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/21 10:25:46 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_strcmp(const	char *s1, const char *s2)
{
	int	res;
	int	i;

	i = 0;
	res = 0;
	if (!s1 || !s2)
		return (-1);
	while (s1[i] && s2[i])
	{
		res = ((unsigned char)s1[i]) - ((unsigned char)s2[i]);
		if (res != 0)
			return (res);
		i++;
	}
	res = ((unsigned char)s1[i]) - ((unsigned char)s2[i]);
	return (res);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 20:05:50 by erodrigu          #+#    #+#             */
/*   Updated: 2015/12/11 06:16:03 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	int		i;
	char	*str;

	i = 0;
	if (s == NULL)
		return (NULL);
	while (s[i])
		i++;
	str = ft_strnew(i);
	i = 0;
	if (f == NULL)
		return (str);
	while (s[i] && str != NULL)
	{
		str[i] = f((unsigned int)i, s[i]);
		i++;
	}
	return (str);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/24 04:31:21 by erodrigu          #+#    #+#             */
/*   Updated: 2016/08/22 08:52:54 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memset(void *b, int c, size_t len)
{
	size_t			i;
	unsigned char	v;
	char			*str;

	v = c;
	i = 0;
	str = b;
	while (i < len)
	{
		str[i] = v;
		i++;
	}
	return (str);
}

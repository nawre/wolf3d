/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 14:37:22 by erodrigu          #+#    #+#             */
/*   Updated: 2016/01/11 14:01:21 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *s1, const char *s2, size_t n)
{
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	if (s2[j] == '\0')
		return ((char *)s1);
	while (s1 && s1[i] && i < n)
	{
		while (s2 && s1[i] == s2[j] && i < n)
		{
			if (s2[j + 1] == '\0')
				return ((char*)&s1[i - j]);
			i++;
			j++;
		}
		i = (i - j + 1);
		j = 0;
	}
	return (NULL);
}

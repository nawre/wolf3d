/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/16 10:38:43 by erodrigu          #+#    #+#             */
/*   Updated: 2016/09/21 17:43:15 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "libft.h"
# include "mlx.h"
# include <math.h>
# include <pthread.h>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>

# define PLHIGH 32
# define ANGLE M_PI_2
# define APR 0.0010471975511966
# define BETA 0.5235987755983
# define PROJ 866
# define NORTH 0x12919A
# define SOUTH 0xCE1313
# define EST 0x890C9C
# define WEST 0x576F2E
# define WHITE 0xFFFFFF

typedef struct	s_pl
{
	int				x;
	int				y;
	int				z;
	double			angle;
}				t_pl;

typedef struct	s_pt
{
	double			x;
	double			y;
	double			angle;
}				t_pt;

typedef struct	s_env
{
	char			**map;
	struct s_pl		*pl;
	void			*mlx;
	void			*win;
	void			*img_ptr;
	char			*image;
	int				size_line;
	int				bpp;
	int				endian;
	int				hmap;
	int				lmap;
}				t_env;

typedef struct	s_thr
{
	struct s_env	*env;
	int				thr;
}				t_thr;

int		keybinding(int key, t_env *env);
int		parsing(char *filemap, t_env *env);
void	paramerror();
int		wolflaunch(t_env *env);
void	initplayer(t_env *env);
void	threading(t_env *env);
void	*raytracing(void *thread);
void	init_thread(t_thr *data, t_env *env, int t);
void	vtracing(t_env *env, double alpha, t_pt *pty);
void	htracing(t_env *env, double alpha, t_pt *ptx);
void	walldistance(t_pt *ptx, t_pt *pty, int pix, t_thr *data);
int		choose_color(int dir, double angle);
void	putwall(int x, int color, double wall, t_env *env);

#endif

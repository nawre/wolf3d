/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calcul.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 12:25:19 by erodrigu          #+#    #+#             */
/*   Updated: 2016/09/20 04:12:49 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include"wolf3d.h"

void	vtracing(t_env *env, double alpha, t_pt *pty)
{
	int			xa;
	double		ya;
	int			xb;
	double		yb;

	if (alpha > ANGLE && alpha < 3 * ANGLE)
	{
		xa = 64;
		xb = (int) (env->pl->x / 64) * 64 + 64;
		yb = env->pl->y - (ft_abs(env->pl->x - xa) * tan(alpha));
	}
	else
	{
		xa = -64;
		xb = (int) (env->pl->x / 64) * 64 - 1;
		yb = env->pl->y + (ft_abs(env->pl->x - xa) * tan(alpha));
	}
	ya = 64 * tan(alpha);

		while((int) (xb / 64) < env->lmap && yb < (env->hmap) &&
			(int) (xb / 64) >= 0 && yb >= 0 &&
			(env->map[xb / 64][(int)yb / 64] != 'o' ||
			env->map[xb / 64][(int)yb / 64] != 's'))
	{
		xb = xb + xa;
		yb = yb + ya;
	}
	pty->x = xb;
	pty->y = yb;
	pty->angle = alpha;
}

void	htracing(t_env *env, double alpha, t_pt *ptx)
{
	int			ya;
	double		xa;
	int			yb;
	double		xb;

	if (alpha > 0 && alpha < M_PI)
	{
		ya = -64;
		yb = (env->pl->x / 64) * 64 - 1;
	}
	else
	{
		yb = (env->pl->x / 64) * 64 + 64;
		ya = 64;
	}
	if (alpha > ANGLE && alpha < 3 * ANGLE)
		xa = env->pl->x + (ft_abs(env->pl->y - ya) / tan(alpha));
	else
		xa = env->pl->x - (ft_abs(env->pl->y - ya) / tan(alpha));
	xb = 64 / tan(alpha);

	while ((int) (xb / 64) < env->lmap && yb < (env->hmap) &&
			(int) (xb / 64) >= 0 && yb >= 0 &&
			(env->map[(int)xb / 64][yb / 64] == 'o' ||
			env->map[(int)xb / 64][yb / 64] == 's'))
	{
		xb = xb + xa;
		yb = yb + ya;
	}
	ptx->x = xb;
	ptx->y = yb;
	ptx->angle = alpha;
}

void	walldistance(t_pt *ptx, t_pt *pty, int pix, t_thr *data)
{
	double		pa;
	double		pb;
	double		dist;
	int			color;
	double		wall;

	pa = ft_abs(data->env->pl->x - ptx->x) / cos(ptx->angle);
	pb = ft_abs(data->env->pl->x - pty->x) / cos(pty->angle);
	if (pa <= pb)
	{
		color = choose_color(1, ptx->angle);
		dist = pa;
	}
	else
	{
		color = choose_color(2, ptx->angle);
		dist = pb;
	}
	if (data->thr <= 4)
		dist = dist * cos(BETA);
	else
		dist = dist * cos(-BETA);
	wall = (64 / dist) * PROJ;
	putwall(pix, color, wall, data->env);
}

int		choose_color(int dir, double angle)
{
	if (dir == 1)
	{
		if (angle > ANGLE && ANGLE < 3 * ANGLE)
			return (WEST);
		else
			return (EST);
	}
	else
	{
		if (angle > 0 && angle < M_PI)
			return (NORTH);
		else
			return (SOUTH);
	}
	return (0);
}

void	putwall(int x, int color, double wall, t_env *env)
{
	int		ymin;
	int		ymax;
	int		y;
	int		col;

	y = 0;
	col = color;
	ymin = 300 - ((int) wall / 2);
	ymax = 300 + ((int) wall / 2);
	//	printf("ymin = %d, ymax = %d\n", ymin, ymax);
	while (y < 600)
	{
		if (y >ymin && y < ymax)
			ft_memcpy(env->image + (x * 4 + y * env->size_line), &col, 4);
		if (x >= ymax)
		{
			color = WHITE;
			ft_memcpy(env->image + (x * 4 + y * env->size_line), &col, 4);
		}
		y++;
	}
}

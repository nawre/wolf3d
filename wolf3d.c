/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/16 10:38:32 by erodrigu          #+#    #+#             */
/*   Updated: 2016/09/21 13:01:15 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		main(int argc, char **argv)
{
	t_env	*env;
	env = malloc(sizeof(t_env));
	if (env == NULL)
		return (-1);
	env->map = NULL;
	env->lmap = 0;
	env->hmap = -64;
	if (argc != 2)
	{
		ft_putstr("Veuillez rentree une et une seule map en parametre.\n");
		return (-1);
	}
	else
		parsing(argv[1], env);
	wolflaunch(env);
	return (0);
}

int		parsing(char *filemap, t_env *env)
{
	int		fd;
	char	*line;
	int		i;

	fd = open(filemap, O_RDONLY);
	if (fd != -1)
	{
		while (get_next_line(fd, &line) > 0)
		{
			env->map = ft_addintab(env->map, line);
			env->hmap += 64;
		}
	}
	close(fd);
	i = 0;
	while (env->map && (*(env->map))[i])
		i++;
	env->lmap = i * 64;
 	return (0);
}

void	paramerror()
{
	int		fd;
	char *line;

	fd = open("ressources/error.txt", O_RDONLY);
	if (fd != -1)
		while (get_next_line(fd, &line))
		{
			ft_putstr(line);
			ft_putchar('\n');
		}
	else 
	{
		ft_putstr("error data on ressources error.txt file unfound !\n");
	}
	exit(0);
}

# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/08/02 21:28:29 by erodrigu          #+#    #+#              #
#    Updated: 2016/09/21 17:45:23 by erodrigu         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = Wolf3d
SRC = wolf3d.c \
	  launch.c \
	  calcul.c \
	  keybinding.c \
 
INC = libft
LIB = libft
OBJ = $(SRC:.c=.o)
CC = gcc
FLAG = -Wall -Wextra -Werror -fsanitize=address -g3
FRAME = -framework OpenGL -framework AppKit

all: $(NAME)

%.o:%.c
	$(CC) $(FLAG) -c $< -o $@ -I $(INC)

$(NAME): $(LIB) $(OBJ)
	$(CC) $(FLAG) -o $(NAME) $(OBJ) -I $(INC) -L $(LIB) -lft -lmlx $(FRAME)

$(LIB):
	$(MAKE) -C $(LIB)

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)

re : fclean all

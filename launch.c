/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   launch.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: erodrigu <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/13 01:22:46 by erodrigu          #+#    #+#             */
/*   Updated: 2016/09/21 17:44:27 by erodrigu         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int		wolflaunch(t_env *env)
{
	env->mlx = mlx_init();
	env->win = mlx_new_window(env->mlx, 1000, 600, "WOLF3D");
	env->img_ptr = mlx_new_image(env->mlx, 1000, 600);
	env->image = mlx_get_data_addr(env->img_ptr, &env->bpp, &env->size_line,
			&env->endian);
	env->pl = malloc(sizeof(t_pl));
	if (env->pl == NULL)
		return (0);
	initplayer(env);
	threading(env);
	mlx_hook(env->win, 2, 1L << 0, &keybinding, env);
	mlx_loop(env->mlx);
	return (0);
}

void	initplayer(t_env *env)
{
	int		x;
	int		y;

	x = 0;;
	env->pl->x = PLHIGH;
	env->pl->y = PLHIGH;
	env->pl->z = PLHIGH;
	env->pl->angle = ANGLE;
	while (env->map && env->map[x])
	{
		y = 0;
		while (env->map[x] && env->map[x][y] != 0 && env->map[x][y] != 's')
			y++;
		if (env->map[x][y] == 's')
		{
			env->pl->x = y * 64 + PLHIGH;
			env->pl->y = x * 64 + PLHIGH;
			env->pl->z = PLHIGH;
			env->pl->angle = ANGLE;
		}
		x++;
	}
}

void	threading(t_env *env)
{
	t_thr		data[8];
	pthread_t	thread[8];
	int			t;

	t = 0;
	while (t < 8)
	{
		init_thread(&data[t], env, t);
		pthread_create(&thread[t], NULL, raytracing, &data[t]);
		t++;
	}
	t = 0;
	while (t < 8)
	{
		pthread_join(thread[t], NULL);
		t++;
	}
	mlx_put_image_to_window(env->mlx, env->win, env->img_ptr, 0, 0);
}

void	init_thread(t_thr *data, t_env *env, int t)
{
	data->env = env;
	data->thr = t;
}

void	*raytracing(void *thread)
{
	t_pt	*ptx;
	t_pt	*pty;
	int		pix;
	double	alpha;
	t_thr	*data;

	ptx = malloc(sizeof(t_pt));
	pty = malloc(sizeof(t_pt));
	data = (t_thr*)thread;
	pix = data->thr * 125;
	alpha = APR * -500  + APR * 125 * data->thr + data->env->pl->angle;
	while (pix < (data->thr + 1) * 125)
	{
		vtracing(data->env, alpha, pty);
		htracing(data->env, alpha, ptx);
		walldistance(ptx, pty, pix, data);
		pix++;
		alpha = alpha + APR;
	}
	free(ptx);
	free(pty);
	return (0);
}
